package com.example.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.R
import com.example.recyclerview.viewholder.MovieImageViewHolder

class Adapter(var imageList: MutableList<Int>) : RecyclerView.Adapter<MovieImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieImageViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false)

        return MovieImageViewHolder(view)
    }

    override fun getItemCount() = imageList.size


    override fun onBindViewHolder(holder: MovieImageViewHolder, position: Int) {
        val img = imageList[position]
        holder.apply {
            recyclerImage.setImageResource(img)
        }
    }
}
package com.example.recyclerview.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.R
import com.example.recyclerview.adapter.Adapter


lateinit var recyclerView: RecyclerView
lateinit var layoutManager: RecyclerView.LayoutManager
lateinit var recyclerAdapter: Adapter
lateinit var list: MutableList<Int>

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(
            R.id.recyclerView
        )
        layoutManager = GridLayoutManager(this, 2)

        list= mutableListOf(R.drawable.i, R.drawable.i1, R.drawable.i2, R.drawable.i3, R.drawable.i4
        ,R.drawable.i5, R.drawable.i6, R.drawable.i7, R.drawable.i8, R.drawable.i9, R.drawable.i11)

        recyclerAdapter= Adapter(list)

        recyclerView.adapter= recyclerAdapter
        recyclerView.layoutManager= layoutManager
    }
}